let slider = document.getElementById('slider');
let style = document.querySelector('[data="slider-thumb"]');
let btnSend = document.getElementById('btnSend');

let emojiList = [
  { title: 'kill', value: 0 },
  { title: 'cry', value: 10 },
  { title: 'tears', value: 20 },
  { title: 'norm', value: 30 },
  { title: 'pleased', value: 60 },
  { title: 'happy', value: 80 }
]

function change() {
  let sliderValue = document.getElementById('slider-value');
  let path = './image/emoji/';
  emojiList.forEach((emoji) => {
    if (slider.value > emoji.value) {
      style.innerHTML = `.slider::-webkit-slider-thumb { background: url(${path + emoji.title + '.png'}) center center no-repeat;}
      .slider::-moz-slider-thumb { background: url(${path + emoji.title + '.png'}) center center no-repeat;}`;
    }
  })

  sliderValue.innerHTML = slider.value;
}

change();
slider.addEventListener('input', change);

function submit() {
  localStorage.setItem(`value${Math.random()}`, slider.value);

  let wrapper = document.getElementById('wrapper');
  wrapper.style.transition = '2000ms';
  wrapper.style.opacity = '0';

  let wrapperWorld = document.getElementById('wrapperWorld');
  wrapperWorld.style.transition = '2000ms';
  wrapperWorld.style.opacity = '1';

  let res = worldRating();
  move(res);
}

function worldRating() {
  let k = 0;
  let res = 0;
  let keys = Object.keys(localStorage);
  for (key of keys) {
    res += +localStorage.getItem(key);
    k++;
  }
  return Math.trunc(res /= k);
}

function changeWorld() {
  let sliderValueWorld = document.getElementById('slider-value_world');
  let sliderWorld = document.getElementById('slider-world');
  let path = './image/emoji/';
  emojiList.forEach((emoji) => {
    if (sliderWorld.value > emoji.value) {
      style.innerHTML = `.slider::-webkit-slider-thumb { background: url(${path + emoji.title + '.png'}) center center no-repeat;}`;
    }
  })

  sliderValueWorld.innerHTML = slider.value;
}

function move(res) {
  let sliderValueWorld = document.getElementById('slider-value_world');
  let sliderWorld = document.getElementById('slider-world');
  let val = 0;

  let idInterval = setInterval(frame, 50);
  function frame() {
    if (val == res) {
      clearInterval(idInterval);
    } else {
      val++;
      changeWorld();
      sliderValueWorld.innerHTML = val;
      sliderWorld.value = val;
    }
  }
}

btnSend.addEventListener('click', submit);
